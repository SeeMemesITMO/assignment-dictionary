flag = felf64
linker = ld
compiler = nasm

main: main.o lib.o dict.o
	$(linker) -o main main.o lib.o dict.o

lib.o: lib.asm
	$(compiler) -$(flags) -o lib.o lib.asm

dict.o: dict.asm
	$(compiler) -$(flags) -o dict.o dict.asm

main.o: main.asm
	$(compiler) -$(flags) -o main.o main.asm

clean:
	rm -rf *.o
rebuild: clean main.o lib.o dict.o

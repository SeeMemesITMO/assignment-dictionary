%define nextElement 0
%macro colon 2
    %%next: dq nextElement
    db %1, 0
    %2:
%define nextElement %%next
%endmacro

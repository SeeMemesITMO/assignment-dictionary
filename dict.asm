global find_word_dict
extern string_equals

section .text

find_word_dict:
    cmp rsi, 0
    je .error
    add rsi, 8
    call string_equals
    test rax, rax
    jnz .findAddress
    sub rsi, 8
    mov rsi, [rsi]
    jmp find_word_dict

.error:
    xor rax, rax
    ret

.findAddress:
    sub rsi, 8
    mov rax, rsi
    ret
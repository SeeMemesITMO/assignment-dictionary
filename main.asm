%include "words.inc"
%define byte 256
%define rbyte 8

global _start
extern print_string
extern print_err_string
extern print_newline
extern string_length
extern exit
extern find_word_dict
extern read_word

section .rodata
not_found: db "key not found", 10, 0
out_of_buffer: db "buffer is overflow", 10, 0
null_line: db 10
section .text
_start:
    mov rdi
    call print_string

    sub rsp, byte
    mov rsi, byte
    mov rdi, rsp
    call read_word
    test rdx, 0
    jg .read_out

    mov rsi, nextElement
    mov rdi, rax
    call find_word_dict
    test rax, rax
    jz .error

    push rax
    mov rdi
    call print_string
    pop rax

    mov rdi, rax
    add rdi, rbyte
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    mov rsi, 1
    call print_string
    mov rdi, null_line
    call print_string
    add rsp, byte
call exit

.read_out:
    mov rdi, out_of_buffer
    call print_err_string
    call exit
ret

.error:
    mov rdi, not_found
    call print_err_string
    call exit
ret
